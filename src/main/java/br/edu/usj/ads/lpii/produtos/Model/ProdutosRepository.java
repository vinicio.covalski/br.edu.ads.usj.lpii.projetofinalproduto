package br.edu.usj.ads.lpii.produtos.Model;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ProdutosRepository extends CrudRepository<Produtos,Long> {
  List<Produtos> findAll();

} 
  
