package br.edu.usj.ads.lpii.produtos.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import br.edu.usj.ads.lpii.produtos.Model.Album;
import br.edu.usj.ads.lpii.produtos.Model.AlbumRepository;
import br.edu.usj.ads.lpii.produtos.Model.Clientes;
import br.edu.usj.ads.lpii.produtos.Model.ClientesRepository;
import br.edu.usj.ads.lpii.produtos.Model.Produtos;
import br.edu.usj.ads.lpii.produtos.Model.ProdutosRepository;

@Controller
public class StoreController {

    @Autowired // Cada classe Repository quando declarada deve vir com uma @Autowired
    ProdutosRepository produtosRepository;

    @Autowired
    ClientesRepository clientesRepository;

    @Autowired
    AlbumRepository albumRepository;

    //
    //

    /// Rotas GET ////

    @GetMapping(value = "/")
    public ModelAndView getDefaut() {

        ModelAndView modelAndView = new ModelAndView("index"); // Criando um construtor chamando o templates/index
        return modelAndView;

    }

    @GetMapping(value = "/clientes")
    public ModelAndView getListClientes() {
        Clientes clientes = new Clientes();

        ModelAndView modelAndView = new ModelAndView("listclientes"); // Criando um construtor chamando o
                                                                      // templates/index
        modelAndView.addObject("clientes", clientes);
        modelAndView.addObject("historico", clientesRepository.findAll());
        return modelAndView;

    }

    @GetMapping(value = "/cadastro_clientes")
    public ModelAndView getCadClientes() {
        Clientes clientes = new Clientes();

        ModelAndView modelAndView = new ModelAndView("cadclientes"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("clientes", clientes);
        return modelAndView;

    }

    @GetMapping(value = "/albuns")
    public ModelAndView getListAlbuns() {
        Album album = new Album();

        ModelAndView modelAndView = new ModelAndView("listalbuns"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("album", album);
        modelAndView.addObject("historico", albumRepository.findAll());
        return modelAndView;

    }

    @GetMapping(value = "/cadastro_albuns")
    public ModelAndView getCadAlbuns() {
        Album album = new Album();

        ModelAndView modelAndView = new ModelAndView("cadalbum"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("album", album);
        return modelAndView;

    }

    @GetMapping(value = "/produtos")
    public ModelAndView getListProdutos() {
        Produtos produto = new Produtos();

        ModelAndView modelAndView = new ModelAndView("listprodutos"); // Criando um construtor chamando o
                                                                      // templates/index
        modelAndView.addObject("produto", produto);
        modelAndView.addObject("historico", produtosRepository.findAll());
        return modelAndView;

    }

    @GetMapping(value = "/cadastro_produtos")
    public ModelAndView getCadProduto() {
        Produtos produtos = new Produtos();
       

        ModelAndView modelAndView = new ModelAndView("cadprodutos"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("produtos", produtos);
       modelAndView.addObject("lista_albuns", albumRepository.findAll());
       System.out.println("TESTESTESTW"+albumRepository.findAll());
 
        return modelAndView;

    }

    @GetMapping(value = "/visualizar_clientes")
    public ModelAndView getVisualizarClientes() {

        ModelAndView modelAndView = new ModelAndView("vclientes"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", clientesRepository.findAll());
        return modelAndView;

    }

    @GetMapping(value = "/visualizar_albuns")
    public ModelAndView getVisualizarAlbuns() {

        ModelAndView modelAndView = new ModelAndView("valbum"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", albumRepository.findAll());
        return modelAndView;

    }

    @GetMapping(value = "/visualizar_produtos")
    public ModelAndView getVisualizarProdutos() {

        ModelAndView modelAndView = new ModelAndView("vproduto"); // Criando um construtor chamando o templates/index
        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", produtosRepository.findAll());
        return modelAndView;

    }

    @GetMapping(value = "/pesquisa")
    public ModelAndView getPesquisa() {

        ModelAndView modelAndView = new ModelAndView("pesquisa"); // Criando um construtor chamando o templates/index
        return modelAndView;

    }
    
    @GetMapping(value = "/pesquisar/{nome}")
    public List<Clientes> getPesquisarNome(@PathVariable String nome) {
        List<Clientes> lista = clientesRepository.findByNomeContainingIgnoreCase(nome);
        return lista;

    }

    
   
    @GetMapping(value = "/contatos")
    public ModelAndView getContatos() {

        ModelAndView modelAndView = new ModelAndView("contatos"); // Criando um construtor chamando o templates/index
        return modelAndView;

    }

    @GetMapping(value = "/visualizar_clientes_detalhes/{id}")
    public ModelAndView getClientesDetalhes(@PathVariable Long id) {

        Clientes clientes = clientesRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("desc_clientes");

        modelAndView.addObject("clientes", clientes);

        return modelAndView;
    }

    @GetMapping(value = "/visualizar_album_detalhes/{id}")
    public ModelAndView getAlbDetalhes(@PathVariable Long id) {

        Album album = albumRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("desc_albuns");

        modelAndView.addObject("album", album);

        return modelAndView;
    }

    @GetMapping(value = "/visualizar_produtos_detalhes/{id}")
    public ModelAndView getProdDetalhetes(@PathVariable Long id) {

        Produtos produtos = produtosRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("desc_produtos");

        modelAndView.addObject("produtos", produtos);

        return modelAndView;
    }

    @GetMapping(value = "/editar_clientes/{id}")
    public ModelAndView getEditarClientes(@PathVariable Long id) {
        // retornar o formulario com a clientes ID preenchida no form
        Clientes clientes = clientesRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("cadclientes");
        modelAndView.addObject("clientes", clientes);
        return modelAndView;
    }

    @GetMapping(value = "/editar_albuns/{id}")
    public ModelAndView getEditarAlbuns(@PathVariable Long id) {
        Album album = albumRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("cadalbum");
        modelAndView.addObject("album", album);
        return modelAndView;
    }

    @GetMapping(value = "/editar_produtos/{id}")
    public ModelAndView getEditarProdutos(@PathVariable Long id) {
        Produtos produtos = produtosRepository.findById(id).get();
        ModelAndView modelAndView = new ModelAndView("cadprodutos");
        modelAndView.addObject("produtos", produtos);
        return modelAndView;
    }

    @GetMapping(value = "/deletar_clientes/{id}")
    public String getDeletarClientes(@PathVariable Long id) {
        clientesRepository.deleteById(id);

          return "redirect:/clientes";
    }


 

    @GetMapping(value = "/deletar_albuns/{id}")
    public String getDeletarAlbuns(@PathVariable Long id) {
        albumRepository.deleteById(id);

        return "redirect:/albuns";
    }

    @GetMapping(value = "/deletar_produtos/{id}")
    public String getDeletarProdutos(@PathVariable Long id) {
        produtosRepository.deleteById(id);

        return "redirect:/produtos";
    }

    @GetMapping(value = "/pesquisa/{nome}")
    public List<Clientes> getPesquisaClientes(@PathVariable String Nome) {
        List<Clientes> lista = clientesRepository.findByNome(Nome);
        return lista;

       
    }

 

    /*
     * @GetMapping(value = "/deleta/visualizar") public ModelAndView getDeletar() {
     * 
     * ModelAndView modelAndView = new ModelAndView("visualizar"); // Criando um
     * construtor chamando o templates/index modelAndView.addObject("historico",
     * operacaoRepository.findAll()); return modelAndView; }
     */

    ///////////// ROTAS POST @PostMappinga anotação mapeia solicitações HTTP POST
    ///////////// para métodos específicos do manipulador. É uma anotação composta
    ///////////// que atua como um atalho para @RequestMapping(method =
    ///////////// RequestMethod.POST)./////

    @PostMapping(value = "/cadastro_clientes")
    public ModelAndView postCadClientes(Clientes clientes) {

        System.out.println("Passei por aqui");

        clientesRepository.save(clientes);

        ModelAndView modelAndView = new ModelAndView("listclientes"); // Criando um construtor chamando o
                                                                      // templates/index

        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", clientesRepository.findAll());

        return modelAndView;

    };

    @PostMapping(value = "/cadastro_produtos")
    public ModelAndView postCadProdutos(Produtos produtos) {
      
        System.out.println("Passei por aqui Produtos");

        produtosRepository.save(produtos);
        
        ModelAndView modelAndView = new ModelAndView("listprodutos"); // Criando um construtor chamando o
                                                                      // templates/index

        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", produtosRepository.findAll());
        modelAndView.addObject("lista_albuns", albumRepository.findAll());
        System.out.println(albumRepository.findAll());
        return modelAndView;
    }

    @PostMapping(value = "/cadastro_albuns")
    public ModelAndView postCadAlbum(Album album) {

        System.out.println("Passei por aqui Album");

        albumRepository.save(album);
        System.out.println(album);

        ModelAndView modelAndView = new ModelAndView("listalbuns"); // Criando um construtor chamando o templates/index

        modelAndView.addObject("mensagem", " Cadastrado com Sucesso");
        modelAndView.addObject("historico", albumRepository.findAll());

        return modelAndView;
    }

}