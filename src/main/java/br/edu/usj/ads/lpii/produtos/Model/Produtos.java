package br.edu.usj.ads.lpii.produtos.Model;

import javax.persistence.Id;
import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
@Entity

public class Produtos {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    Long id;
    
    String musica;
    String album;
    String artista;
    String gravadora;
    Integer ano;
    Time duracao;
    Double preco; 
  

}